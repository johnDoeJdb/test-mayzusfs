<?php

namespace App\Helper;

class ParameterManager
{
    private static $parameters;

    public static function getParameter($category, $parameter)
    {
        self::init();
        if (array_key_exists($category, self::$parameters) && array_key_exists($parameter, self::$parameters[$category])) {
            $value = self::$parameters[$category][$parameter];
        } else {
            $value = null;
        }

        return $value;
    }

    private static function init()
    {
        $parametersPath = BASE_DIR.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'parameters.xml';
        if (!self::$parameters) {
            if (file_exists($parametersPath)) {
                $xml = file_get_contents($parametersPath);
                self::$parameters = json_decode(json_encode((array) simplexml_load_string($xml)), 1);;
            } else {
                throw new \Exception('Parameter file does not exist');
            }
        }
    }
}





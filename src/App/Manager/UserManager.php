<?php

namespace App\Manager;

use App\Database\DatabaseConnector;

class UserManager
{
    public function registerUser($username, $password)
    {
        $connection = DatabaseConnector::getConnection();
        $sth = $connection->prepare('
            INSERT INTO users (username,password)
            VALUES (?, ?);
        ');
        $sth->bindParam(1, $username, \PDO::PARAM_STR);
        $sth->bindParam(2, sha1($password), \PDO::PARAM_STR);
        $sth->execute();
    }

    public function updateUser($username, $password)
    {
        $connection = DatabaseConnector::getConnection();
        $sth = $connection->prepare("
            UPDATE users
            SET username=?, password=?
            WHERE username=?;
        ");
        $sth->bindParam(1, $username, \PDO::PARAM_STR);
        $sth->bindParam(2, $password, \PDO::PARAM_STR);
        $sth->bindParam(3, $username, \PDO::PARAM_STR);
        $sth->execute();
    }

    public function findUser($username)
    {
        $connection = DatabaseConnector::getConnection();
        $sth = $connection->prepare('
            SELECT * FROM users
            WHERE username = ?
            ;
        ');
        $sth->bindParam(1, $username, \PDO::PARAM_STR);
        $sth->execute();

        return $sth->fetch();
    }

    public function checkPassword($username, $password)
    {
        $user = $this->getUser($username);
        if ($user['password'] === sha1($password)) {
            $result = true;
        } else {
            $result = false;
        };

        return $result;
    }

    public function getUser($username)
    {
        $connection = DatabaseConnector::getConnection();
        $sth = $connection->prepare('
            SELECT * FROM users
            WHERE username = ?
            ;
        ');
        $sth->bindParam(1, $username, \PDO::PARAM_STR);
        $sth->execute();

        return $sth->fetch();
    }
}





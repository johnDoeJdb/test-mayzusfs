<?php

namespace App\Database;

use App\Helper\ParameterManager;

class DatabaseConnector
{
    public static function getConnection()
    {
        $host = ParameterManager::getParameter('database', 'host');
        $databaseName = ParameterManager::getParameter('database', 'databaseName');
        $username = ParameterManager::getParameter('database', 'username');
        $password = ParameterManager::getParameter('database', 'password');

        return new \PDO('mysql:host='.$host.';dbname='.$databaseName, $username, $password);
    }
}
<?php

// initialization application
define('BASE_DIR', __DIR__);
define('SRC_DIR', __DIR__.DIRECTORY_SEPARATOR.'src');
define('APP_DIR', SRC_DIR.DIRECTORY_SEPARATOR.'App');
require_once BASE_DIR.'/classLoader.php';
$classLoader = new SplClassLoader(null, SRC_DIR);
$classLoader->register();

// Run application
session_start();
if (!array_key_exists('auth', $_SESSION)) {
    if (array_key_exists('username', $_POST) && array_key_exists('password', $_POST)) {
        $userManager = new \App\Manager\UserManager();
        if ($userManager->checkPassword($_POST['username'], $_POST['password'])) {
            $_SESSION['auth'] = true;
            header("Refresh:0");
        } else {
            echo 'Access denied';
        }
    } else {
        echo $loginForm = file_get_contents(APP_DIR.'/View/LoginForm.html');
    }
} else {
    echo 'You logged in';
}
